<h3>Upload task image</h3>

<?php if ($data['message'] === 'success'): ?>
    <p style="color: green;">Image successfully uploaded.</p>
<?php elseif ($data['message'] === 'error'): ?>
    <p style="color: red;">Error acquired while uploading image.</p>
<?php elseif ($data['message'] === 'validation_error'): ?>
    <p style="color: red;">Validation error.</p>
<?php endif; ?>

<form enctype="multipart/form-data" method="post" action="#">
    <div><input type="file" name="task_image_upload" id="task_image_upload" multiple="false"></div>
    <?php wp_nonce_field('task_image_upload', 'task_image_upload_nonce'); ?>
    <div><label for="selectSize">Image resize</label></div>
    <div>
        <select id="selectSize" name="task_image_size">
            <option value="<?php echo get_option('thumbnail_size_w');?>"><?php echo get_intermediate_image_sizes()[0];?></option>
            <option value="<?php echo get_option('medium_size_w');?>"><?php echo get_intermediate_image_sizes()[1];?></option>
            <option value="<?php echo get_option('large_size_w');?>"><?php echo get_intermediate_image_sizes()[2];?></option>
        </select>
    </div>
    <div><input type="submit" name="task_image_submit" id="task_image_submit" class="button button-primary"
                value="Upload"/>
    </div>
</form>
