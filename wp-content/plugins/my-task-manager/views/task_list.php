<?php $classes = [
    'Open'        => '',
    'In progress' => 'text-primary',
    'Completed'   => 'text-success'
]; ?>

<?php if ($data['userTasks']): ?>
    <?php $user = wp_get_current_user(); ?>
    <h3>My tasks</h3>
    <table class="table table-striped table-borderless border">
        <thead>
        <tr>
            <th style="width: 10%;">Title</th>
            <th style="width: 20%;">Image</th>
            <th style="width: 30%;">Content</th>
            <th style="width: 20%;">Status <span id="message" style="color: green; font-size: 14px; display: none;">(updated)</span></th>
            <th style="width: 20%;">Author</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data['userTasks'] as $task): ?>
            <?php $status = get_post_meta($task->ID, '_mtm_task_status', true); ?>
            <tr>
                <td><?php echo $task->post_title; ?></td>
                <td><?php echo get_the_post_thumbnail($task->ID, 'eshop-task-image'); ?></td>
                <td><?php echo $task->post_content; ?></td>
                <td>
                    <select name="mtm_field" id="mtm_field" class="task-status <?php echo $classes[$status]; ?>"
                            data-post-id="<?php echo $task->ID; ?>">
                        <option value='Open' <?php selected($status, 'Open'); ?>>Open</option>
                        <option value='In progress' <?php selected($status, 'In progress'); ?>>In progress</option>
                        <option value='Completed' <?php selected($status, 'Completed'); ?>>Completed</option>
                    </select>
                </td>
                <td><?php echo $user->user_nicename; ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>

<h3>All tasks</h3>
<table class="table table-striped table-borderless border">
    <thead>
    <tr>
        <th style="width: 10%;">Title</th>
        <th style="width: 20%;">Image</th>
        <th style="width: 30%;">Content</th>
        <th style="width: 20%;">Status <span id="message" style="color: green; font-size: 14px; display: none;">(updated)</span></th>
        <th style="width: 20%;">Author</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($data['tasks'] as $task): ?>
        <?php
        $status = get_post_meta($task->ID, '_mtm_task_status', true);
        $user = get_user_by('ID', $task->post_author);
        ?>
        <tr>
            <td><?php echo $task->post_title; ?></td>
            <td><?php echo get_the_post_thumbnail($task->ID, 'eshop-task-image'); ?></td>
            <td><?php echo $task->post_content; ?></td>
            <td><span class="<?php echo $classes[$status]; ?>"><?php echo $status; ?></span></td>
            <td><?php echo $user->user_nicename; ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<script type="text/javascript">
    jQuery(document).ready(function ($) {
        var ajaxUrl = "<?php echo admin_url('admin-ajax.php'); ?>";

        $('.task-status').on('click', function () {
            var select = $(this);
            var status = select.val();
            var post_id = select.data('post-id');
            $.post(
                ajaxUrl,
                {
                    'action': 'change_task_status',
                    'post_id': post_id,
                    'value': status
                },
                function (response) {

                    switch (status) {
                        case 'In progress':
                            select.removeClass('text-success');
                            select.addClass('text-primary');
                            break;
                        case 'Completed':
                            select.removeClass('text-primary');
                            select.addClass('text-success');
                            break;
                        default:
                            select.removeClass('text-success');
                            select.removeClass('text-primary');
                            break;
                    }

                    if (response) {
                        $("#message").show().delay(1000).fadeOut();
                    }
                }
            );
        });
    });
</script>