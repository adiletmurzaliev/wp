<?php $value = get_post_meta($data['post']->ID, '_mtm_task_status', true); ?>
<label for="mtm_field">Task status</label><br>
<select name="mtm_field" id="mtm_field" class="postbox">
    <option value="Open" <?php selected($value, 'Open'); ?>>Open</option>
    <option value="In progress" <?php selected($value, 'In progress'); ?>>In progress</option>
    <option value="Completed" <?php selected($value, 'Completed'); ?>>Completed</option>
</select>