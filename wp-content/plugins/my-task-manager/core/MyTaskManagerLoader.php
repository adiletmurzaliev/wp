<?php

namespace mtm\core;

use mtm\controllers\MyTaskManager;

class MyTaskManagerLoader
{
    public function __construct()
    {
        register_activation_hook(MTM_PLUGIN, [$this, 'activate']);
        register_deactivation_hook(MTM_PLUGIN, [$this, 'deactivate']);

        $this->initControllers();
    }

    public function initControllers()
    {
        new MyTaskManager();
    }

    public function activate()
    {
        $page = get_page_by_title('Tasks');

        if (!$page) {
            wp_insert_post([
                'post_title'   => 'Tasks',
                'post_content' => '[task_list]',
                'post_type'    => 'page',
                'post_author'  => get_current_user_id(),
                'post_status'  => 'publish'
            ]);
        }
    }

    public function deactivate()
    {
        $page = get_page_by_title('Tasks');
        wp_delete_post($page->ID, true);
    }
}