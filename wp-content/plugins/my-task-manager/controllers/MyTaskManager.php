<?php

namespace mtm\controllers;

class MyTaskManager
{
    public function __construct()
    {
        add_action('init', [$this, 'registerTaskPostType'], 0);
        add_action('add_meta_boxes', [$this, 'addMetaStatusFormBox']);
        add_action('save_post', [$this, 'saveStatusFormBoxData']);
        add_filter('manage_task_posts_columns', [$this, 'smashingTaskColumns']);
        add_action('manage_task_posts_custom_column', [$this, 'smashingTaskColumn'], 10, 2);
        add_shortcode('task_list', [$this, 'taskList']);
        add_action('wp_ajax_change_task_status', [$this, 'changeTaskStatusAjax']);
        add_filter('wp_dropdown_users_args', [$this, 'addSubscribersToDropdown'], 10, 2);
        add_action('admin_menu', [$this, 'taskImagesMenu']);
    }

    public function taskImagesMenu()
    {
        add_submenu_page('upload.php', 'Add task image', 'Add task image', 'manage_options', 'task-images', [$this, 'taskImagesMenuContent']);
    }

    public function taskImagesMenuContent()
    {
        $message = null;

        if (isset($_POST['task_image_submit'])) {
            if (
                isset($_POST['task_image_upload_nonce'])
                && wp_verify_nonce($_POST['task_image_upload_nonce'], 'task_image_upload')
                && $_FILES['task_image_upload']['size'] !== 0
                && isset($_POST['task_image_size'])
            ) {
                try {
                    $image = new \Imagick($_FILES['task_image_upload']['tmp_name']);
                    $image->thumbnailImage((int)$_POST['task_image_size'], 0);
                    $image->writeImage($_FILES['task_image_upload']['tmp_name']);
                } catch (\ImagickException $e) {
                    echo $e->getMessage();
                }
                $result = media_handle_upload('task_image_upload', 0);

                if ($result) {
                    $message = 'success';
                } else {
                    $message = 'error';
                }
            } else {
                $message = 'validation_error';
            }
        }

        $this->render('task_image_upload.php', ['message' => $message]);
    }

    public function registerTaskPostType()
    {
        $labels = [
            'name'                  => _x('Task Manager', 'Post Type General Name', 'mtm'),
            'singular_name'         => _x('Task Manager', 'Post Type Singular Name', 'mtm'),
            'menu_name'             => __('Task Manager', 'mtm'),
            'name_admin_bar'        => __('Post Type', 'mtm'),
            'archives'              => __('Item Archives', 'mtm'),
            'parent_item_colon'     => __('Parent Item:', 'mtm'),
            'all_items'             => __('All Tasks', 'mtm'),
            'add_new_item'          => __('Add new Task ', 'mtm'),
            'add_new'               => __('Add Task ', 'mtm'),
            'new_item'              => __('New Task', 'mtm'),
            'edit_item'             => __('View', 'mtm'),
            'update_item'           => __('Update Task', 'mtm'),
            'view_item'             => __('View Task', 'mtm'),
            'search_items'          => __('Search Task', 'mtm'),
            'not_found'             => __('Not found', 'mtm'),
            'not_found_in_trash'    => __('Not found in Trash', 'mtm'),
            'featured_image'        => __('Featured Image', 'mtm'),
            'set_featured_image'    => __('Set featured image', 'mtm'),
            'remove_featured_image' => __('Remove featured image', 'mtm'),
            'use_featured_image'    => __('Use as featured image', 'mtm'),
            'insert_into_item'      => __('Insert into task', 'mtm'),
            'uploaded_to_this_item' => __('Uploaded to this item', 'mtm'),
            'items_list'            => __('Tasks list', 'mtm'),
            'items_list_navigation' => __('Items list navigation', 'mtm'),
            'filter_items_list'     => __('Filter items list', 'mtm'),
        ];

        $args = array(
            'label'               => __('Task Manager', 'mtm'),
            'description'         => __('Task manager', 'mtm'),
            'labels'              => $labels,
            'supports'            => ['title', 'author', 'editor', 'mtm', 'thumbnail'],
            'taxonomies'          => array(),
            'hierarchical'        => true,
            'public'              => false,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'menu_position'       => 25,
            'menu_icon'           => 'dashicons-welcome-write-blog',
            'show_in_admin_bar'   => true,
            'show_in_nav_menus'   => true,
            'can_export'          => true,
            'has_archive'         => false,
            'exclude_from_search' => false,
            'publicly_queryable'  => false,
            'capability_type'     => 'post',
            'capabilities'        => [],
            'map_meta_cap'        => true
        );
        register_post_type('task', $args);
    }

    public function addMetaStatusFormBox()
    {
        add_meta_box(
            'mtm_box_id',
            'Task info',
            [$this, 'renderStatusFormBox'],
            'task',
            'side'
        );
    }

    /**
     * @param $post
     */
    public function renderStatusFormBox($post)
    {
        $this->render('box_html.php', [
            'post' => $post
        ]);
    }

    /**
     * @param $post_id
     */
    public function saveStatusFormBoxData($post_id)
    {
        if (array_key_exists('mtm_field', $_POST)) {
            update_post_meta(
                $post_id,
                '_mtm_task_status',
                $_POST['mtm_field']
            );
        }
    }

    /**
     * @param $columns
     *
     * @return array
     */
    public function smashingTaskColumns($columns)
    {

        $columns = array(
            'cb'     => $columns['cb'],
            'title'  => __('Title'),
            'status' => __('Status'),
            'author' => __('Author', 'smashing'),
            'date'   => __('Date', 'smashing'),
        );

        return $columns;
    }

    /**
     * @param $column
     * @param $post_id
     */
    public function smashingTaskColumn($column, $post_id)
    {
        if ('status' === $column) {
            $status = get_post_meta($post_id, '_mtm_task_status', true);
            switch ($status) {
                case 'Open':
                    $color = 'gray';
                    break;
                case 'In progress':
                    $color = 'blue';
                    break;
                case 'Completed':
                    $color = 'green';
                    break;
                default:
                    $color = 'gray';
            }

            echo "<span style='color: {$color}'>{$status}</span>";
        }
    }

    public function taskList()
    {

        $userTasks = null;

        if (is_user_logged_in()) {
            $userTasks = get_posts(['post_type' => 'task', 'author' => get_current_user_id()]);
            $tasks = get_posts(['post_type' => 'task', 'author' => -get_current_user_id()]);
        } else {
            $tasks = get_posts(['post_type' => 'task']);
        }

        return $this->render('task_list.php', [
            'userTasks' => $userTasks,
            'tasks'     => $tasks
        ], false);
    }

    public function changeTaskStatusAjax()
    {
        $post = get_post($_POST['post_id']);
        if ($post) {
            if ($post->post_author == get_current_user_id()) {
                if (update_post_meta($_POST['post_id'], '_mtm_task_status', $_POST['value'])) {
                    wp_die(true);
                }
            }
        }

        wp_die(false);
    }

    /**
     * @param $query_args
     * @param $r
     *
     * @return mixed
     */
    public function addSubscribersToDropdown($query_args, $r)
    {
        $query_args['who'] = '';
        return $query_args;
    }

    /**
     * @param $template
     * @param $data
     * @param bool $echo
     *
     * @return bool|false|string
     */
    private function render($template, $data, $echo = true)
    {
        ob_start();
        include MTM_ROOT . '/views/' . $template;

        if ($echo) {
            echo ob_get_clean();
            return true;
        } else {
            return ob_get_clean();
        }
    }

}