<?php
/*
Plugin Name: My Task Manager
Plugin URI:
Description: Simple task manager
Author: Adilet
Version: 1.0
Author URI:
*/


define('MTM_ROOT', dirname(__FILE__));
define('MTM_PLUGIN', __FILE__);

require_once(MTM_ROOT . '/core/Autoload.php');

use mtm\core\MyTaskManagerLoader;

new MyTaskManagerLoader();