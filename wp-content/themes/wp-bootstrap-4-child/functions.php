<?php

add_action('wp_enqueue_scripts', 'my_theme_enqueue_styles');

function my_theme_enqueue_styles()
{
    wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css');
}

remove_action('woocommerce_before_main_content', 'wp_bootstrap_4_woocommerce_wrapper_before');

add_action( 'get_header', 'wc_remove_sidebar' );

function wc_remove_sidebar() {
    if ( is_product() ) {
        remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );
    }
}


function wp_bootstrap_4_woocommerce_wrapper_before()
{
    ?>
    <div class="container">
    <div class="row">
    <?php if (get_theme_mod('default_sidebar_position', 'right') === 'no') : ?>
        <div class="col-md-12 wp-bp-content-width">
    <?php else : ?>
        <div class="col-md-12 wp-bp-content-width">
    <?php endif; ?>
    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
            <div class="mt-3r">
    <?php
}

function wp_bootstrap_4_woocommerce_wrapper_after()
{
		?>
								</div>
							</main><!-- #main -->
						</div><!-- #primary -->
					</div>
					<!-- /.col-md-8 -->
				</div>
				<!-- /.row -->
			</div>
			<!-- /.container -->
		<?php
}

function wc_check_active_endpoint($endpoint)
{
    $endpointFromUrl = WC()->query->get_current_endpoint();

    if($endpointFromUrl === $endpoint){
        return true;
    } elseif ($endpointFromUrl === '' && $endpoint === 'dashboard'){
        return true;
    }

    return false;
}