<?php

define('THEME_ROOT', get_stylesheet_directory_uri());

function eShopStyles()
{
    wp_enqueue_style('animate-css', THEME_ROOT . '/assets/css/animate.css', [], null);
    wp_enqueue_style('icomoon-css', THEME_ROOT . '/assets/css/icomoon.css', [], null);
    wp_enqueue_style('ionicons-css', THEME_ROOT . '/assets/css/ionicons.min.css', [], null);
    wp_enqueue_style('bootstrap-css', THEME_ROOT . '/assets/css/bootstrap.min.css', [], null);
    wp_enqueue_style('magnific-popup-css', THEME_ROOT . '/assets/css/magnific-popup.css', [], null);
    wp_enqueue_style('flexslider-css', THEME_ROOT . '/assets/css/flexslider.css', [], null);
    wp_enqueue_style('owl.carousel-css', THEME_ROOT . '/assets/css/owl.carousel.min.css', [], null);
    wp_enqueue_style('owl-theme-default-css', THEME_ROOT . '/assets/css/owl.theme.default.min.css', [], null);
    wp_enqueue_style('bootstrap-datepicker-css', THEME_ROOT . '/assets/css/bootstrap-datepicker.css', [], null);
    wp_enqueue_style('flaticon-css', THEME_ROOT . '/assets/fonts/flaticon/font/flaticon.css', [], null);
    wp_enqueue_style('style-css', THEME_ROOT . '/assets/css/style.css', [], null);
}

function eShopScripts()
{
    wp_enqueue_script('jquery-js', THEME_ROOT . '/assets/js/jquery.min.js', [], null, true);
    wp_enqueue_script('popper-js', THEME_ROOT . '/assets/js/popper.min.js', ['jquery-js'], null, true);
    wp_enqueue_script('bootstrap-js', THEME_ROOT . '/assets/js/bootstrap.min.js', ['jquery-js'], null, true);
    wp_enqueue_script('jquery-easing-js', THEME_ROOT . '/assets/js/jquery.easing.1.3.js', ['jquery-js'], null, true);
    wp_enqueue_script('jquery-waypoints-js', THEME_ROOT . '/assets/js/jquery.waypoints.min.js', ['jquery-js'], null, true);
    wp_enqueue_script('jquery-flexslider-js', THEME_ROOT . '/assets/js/jquery.flexslider-min.js', ['jquery-js'], null, true);
    wp_enqueue_script('owl-carousel-js', THEME_ROOT . '/assets/js/owl.carousel.min.js', ['jquery-js'], null, true);
    wp_enqueue_script('jquery-magnific-popup-js', THEME_ROOT . '/assets/js/jquery.magnific-popup.min.js', ['jquery-js'], null, true);
    wp_enqueue_script('magnific-popup-options-js', THEME_ROOT . '/assets/js/magnific-popup-options.js', ['jquery-js'], null, true);
    wp_enqueue_script('bootstrap-datepicker-js', THEME_ROOT . '/assets/js/bootstrap-datepicker.js', ['jquery-js'], null, true);
    wp_enqueue_script('jquery-stellar-js', THEME_ROOT . '/assets/js/jquery.stellar.min.js', ['jquery-js'], null, true);
    wp_enqueue_script('main-js', THEME_ROOT . '/assets/js/main.js', ['jquery-js'], null, true);
}

add_action('wp_enqueue_scripts', 'eShopStyles');
add_action('wp_enqueue_scripts', 'eShopScripts');

register_nav_menus(array(
    'menu-1' => esc_html__('Primary', 'eshop'),
));

add_theme_support('post-thumbnails');
add_image_size('eshop-task-image', 100, 100, true);