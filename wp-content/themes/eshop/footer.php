<?php

?>

<footer id="colophon" class="site-footer" role="contentinfo">
    <div class="wrap">
        <?php
        get_template_part('template-parts/footer/footer', 'widgets');

        if (has_nav_menu('social')) : ?>
            <nav class="social-navigation" role="navigation"
                 aria-label="<?php esc_attr_e('Footer Social Links Menu', 'twentyseventeen'); ?>">
                <?php
                wp_nav_menu(array(
                    'theme_location' => 'social',
                    'menu_class'     => 'social-links-menu',
                    'depth'          => 1,
                    'link_before'    => '<span class="screen-reader-text">',
                    'link_after'     => '</span>' . twentyseventeen_get_svg(array('icon' => 'chain')),
                ));
                ?>
            </nav><!-- .social-navigation -->
        <?php endif;

        get_template_part('template-parts/footer/site', 'info');
        ?>
    </div><!-- .wrap -->
</footer><!-- #colophon -->

<footer id="colorlib-footer" role="contentinfo">
    <div class="copy">
        <div class="row">
            <div class="col-sm-12 text-center">
                <p>
							<span><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i
                                        class="icon-heart" aria-hidden="true"></i> by <a href="https://colorlib.com"
                                                                                         target="_blank">Colorlib</a>
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></span>
                    <span class="block">Demo Images: <a href="http://unsplash.co/" target="_blank">Unsplash</a> , <a
                                href="http://pexels.com/" target="_blank">Pexels.com</a></span>
                </p>
            </div>
        </div>
    </div>
</footer>
</div><!-- #page -->
<?php wp_footer(); ?>

</body>
</html>
