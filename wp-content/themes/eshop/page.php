<?php

get_header(); ?>

    <div class="colorlib mt-4">
        <div class="container">
            <div class="about-wrap">
                <h2><?php the_title();?></h2>
                <hr>
                <div class="content">
                    <?php
                    while ( have_posts() ) : the_post();

                        the_content();

                        // If comments are open or we have at least one comment, load up the comment template.
                        if ( comments_open() || get_comments_number() ) :
                            comments_template();
                        endif;

                    endwhile; // End of the loop.
                    ?>
                </div>
            </div>
        </div>
    </div>

<?php get_footer();
